<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'betascxs_beyondbasicshairsalon' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'UjB ! *hbutG>_oxhv+sk)99>I~_jtbfMo:wBfR%(9x1WKb+Yt0)H,)Ovr=:VDVN' );
define( 'SECURE_AUTH_KEY',  'dI+q}}O2jol)pbfW~oXixgdi~IJdu8[^k_nY5QI!DMem(A4ZVK>PY}O.Hvfvh=7/' );
define( 'LOGGED_IN_KEY',    '3n3I+74{N1^i+(l7i?>2q8($R!Yc%l2[laBpW@u_tiOlazexL_^9DCX[ouQ5*_gB' );
define( 'NONCE_KEY',        'KL|y$>p6:=#w.t%n2%@q#;yy&!@p mq.J/7U.J?Uchvox/^y#NVVX,U*KuM1ZM,1' );
define( 'AUTH_SALT',        ' ?-IAXOpcB~#qo.yIB+JvEIjw-qT-]w,1=A:2mKx{gi*r-3:G>tY0<aQo.3SQ9]]' );
define( 'SECURE_AUTH_SALT', 'Xh91eQXesDdvsf(tA5>&K |Hp9~i^aU^rYwuN@?3?V&l6u|2?I<d?eV`eBEi^fae' );
define( 'LOGGED_IN_SALT',   'Se6ShlZ1jUoXbU o3HUl{%]m`Q|R -Edr&j?N<}8!RKTN1H:*}596_mL)2`44VsD' );
define( 'NONCE_SALT',       'X{lr,62bx7`Ml_Oa8pXL>$k, i[ 5VL6B8*F{SvcumU=`UKW>UMl#p$T._C*&@=l' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'betascxs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

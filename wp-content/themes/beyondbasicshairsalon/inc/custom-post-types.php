<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'beyondbasicshairsalon' ),
		'singular_name'			=> __( 'Fence', 'beyondbasicshairsalon' ),
		'add_new'				=> __( 'New Fence', 'beyondbasicshairsalon' ),
		'add_new_item'			=> __( 'Add New Fence', 'beyondbasicshairsalon' ),
		'edit_item'				=> __( 'Edit Fence', 'beyondbasicshairsalon' ),
		'new_item'				=> __( 'New Fence', 'beyondbasicshairsalon' ),
		'view_item'				=> __( 'View Fence', 'beyondbasicshairsalon' ),
		'search_items'			=> __( 'Search Fences', 'beyondbasicshairsalon' ),
		'not_found'				=>  __( 'No Fences Found', 'beyondbasicshairsalon' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'beyondbasicshairsalon' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'beyondbasicshairsalon' ),
		'singular_name'		=> __( 'Category', 'beyondbasicshairsalon' ),
		'search_items'		=> __( 'Search Categories', 'beyondbasicshairsalon' ),
		'all_items'			=> __( 'All Categories', 'beyondbasicshairsalon' ),
		'edit_item'			=> __( 'Edit Category', 'beyondbasicshairsalon' ),
		'update_item'		=> __( 'Update Category', 'beyondbasicshairsalon' ),
		'add_new_item'		=> __( 'Add New Category', 'beyondbasicshairsalon' ),
		'new_item_name'		=> __( 'New Category Name', 'beyondbasicshairsalon' ),
		'menu_name'			=> __( 'Categories', 'beyondbasicshairsalon' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );
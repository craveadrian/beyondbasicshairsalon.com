<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Beyond_Basic_Hair_Salon
 * @since 1.0.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'beyondbasicshairsalon'); ?></a>

		<header id="masthead" class="site-header">
			<div class="site-branding-container container">
				<div class="site-header__wrap">
					<?php if (has_custom_logo()) : ?>
						<div class="site-header__logo"><?php the_custom_logo(); ?></div>
					<?php endif; ?>
					<div class="site-header__nav">
						<?php get_template_part('template-parts/navigation/navigation', 'top'); ?>
					</div>
					<div class="site-header__info">
						<?php if (checkoption('phone')) :
							echo do_shortcode('[beyondbasicshairsalon_option var="phone" type="link" text="" link_type="phone" class="site-header__phone"]');
						endif; ?>
						<?php if (checkoption('email')) :
							echo do_shortcode('[beyondbasicshairsalon_option var="email" type="link" text="" link_type="email" class="site-header__email"]');
						endif; ?>
					</div>
				</div>
			</div><!-- .layout-wrap -->

		</header><!-- #masthead -->

		<?php get_template_part('template-parts/header/site', 'banner'); ?>

		<div id="content" class="site-content">
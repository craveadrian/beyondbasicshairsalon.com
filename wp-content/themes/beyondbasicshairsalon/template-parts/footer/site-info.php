<div class="copyright">
	<?php
		$site_info = get_bloginfo( 'description' ) . ' - ' . get_bloginfo( 'name' ) . ' &copy; ' . date( 'Y' );
		$privacy_policy = '<a href="' . site_url() . '/privacy-policy"> Privacy Policy </a>'; 
		if ( get_theme_mod( 'copyright' ) ) :
			echo get_theme_mod( 'copyright' ) . $privacy_policy;
		else :
			echo $site_info;
		endif;
	?>
</div>

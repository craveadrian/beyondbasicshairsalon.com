<?php

/**
 * Displays header site banner area
 *
 * @package WordPress
 * @subpackage Beyond_Basic_Hair_Salon
 * @since 1.0.0
 */
?>

<?php if (get_theme_mod('banner_image') || get_theme_mod('banner_slider') ||  get_theme_mod('banner_background')) : ?>
	<?php if (is_front_page()) : ?>
		<div class="site-banner">
			<div class="site-banner__container">
				<?php if (get_theme_mod('banner_slider')) : $s_code = get_theme_mod('banner_slider'); ?>
					<div class="site-banner__slider">
						<?php echo do_shortcode($s_code); ?>
					</div>
					<div class="site-banner__img-holder">
						<img src="<?php echo get_theme_mod('banner_image'); ?>" alt="image">
					</div>
				<?php else : ?>
					<?php if (get_theme_mod('banner_background')) : ?>
						<div class="site-banner__slider">
							<img src="<?php echo get_theme_mod('banner_background'); ?>" alt="image">
						</div>
						<div class="site-banner__img-holder">
							<img src="<?php echo get_theme_mod('banner_image'); ?>" alt="image">
						</div>
					<?php endif; ?>

				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- .site-banner -->
<?php endif; ?>
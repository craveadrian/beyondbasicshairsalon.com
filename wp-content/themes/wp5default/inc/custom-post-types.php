<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'wp5default' ),
		'singular_name'			=> __( 'Fence', 'wp5default' ),
		'add_new'				=> __( 'New Fence', 'wp5default' ),
		'add_new_item'			=> __( 'Add New Fence', 'wp5default' ),
		'edit_item'				=> __( 'Edit Fence', 'wp5default' ),
		'new_item'				=> __( 'New Fence', 'wp5default' ),
		'view_item'				=> __( 'View Fence', 'wp5default' ),
		'search_items'			=> __( 'Search Fences', 'wp5default' ),
		'not_found'				=>  __( 'No Fences Found', 'wp5default' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'wp5default' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'wp5default' ),
		'singular_name'		=> __( 'Category', 'wp5default' ),
		'search_items'		=> __( 'Search Categories', 'wp5default' ),
		'all_items'			=> __( 'All Categories', 'wp5default' ),
		'edit_item'			=> __( 'Edit Category', 'wp5default' ),
		'update_item'		=> __( 'Update Category', 'wp5default' ),
		'add_new_item'		=> __( 'Add New Category', 'wp5default' ),
		'new_item_name'		=> __( 'New Category Name', 'wp5default' ),
		'menu_name'			=> __( 'Categories', 'wp5default' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );